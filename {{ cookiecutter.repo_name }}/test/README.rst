Testing
=======

This directory is where we keep all the different checks we have
encountered or thought up to ensure that the code doesn't break
as we continuously make changes to it. Contributions welcome!  

Running
-------

To run the whole suite of tests, simply run::

   pytest

In this ``test`` directory, or one level up in the top-level of 
the repository.  

It's good to get in the habit of running the tests before either
committing or pushing, to ensure that you don't inflict your
broken code on anyone else.  

Contributing
------------

``pytest`` is our testing framework of choice, but the written tests 
*should* work with most other Python testing packages, such as 
``unittest`` and ``nose``.  

To add your own tests, just put them in this folder, either in an
existing test script or in a new one named ``test_``-something, and
containing classes, methods, or functions also containing the word
``test``. Pytest will find them automatically 99% of the time.  

For examples of tests, test fixtures, and test parameters, see 
``test_JobConfig.py``.
