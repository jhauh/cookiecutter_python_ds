import pytest
import jsonschema
from {{ cookiecutter.package_name }}.config import JobConfig


@pytest.fixture
def config():
    """ A basic dictionary config. """
    conf = {
        'prefix': 'Z:/',
        'transform_opts': {},
        'data': {
            'input': {
                'filepath_or_buffer': 'data.csv',
                'sep': ',',
            }
        }
    }
    return conf


@pytest.fixture
def JobConf(config):
    """ Instantiated JobConfig object. """
    return JobConfig(config)


def test_jobconfig_inputerrors():
    with pytest.raises(ValueError):
        c = JobConfig(1)


def test_jobconfig_assignment_simple():
    c = JobConfig({'data': 'data.csv'})
    assert c.data == 'data.csv'


def test_jobconfig_assignment_full(config):
    c = JobConfig(config)
    assert c.data['input']['filepath_or_buffer'] == 'data.csv'


def test_jobconfig_schemaerror1():
    # does the config file contain a data entry
    with pytest.raises(jsonschema.exceptions.ValidationError):
        c = JobConfig({'hello': 'data.csv'}, validate_config=True)


def test_jobconfig_schemaerror2():
    # has data, but no pandas args
    with pytest.raises(jsonschema.exceptions.ValidationError):
        c = JobConfig({'data': 'data.csv'}, validate_config=True)


def test_jobconfig_prepend(config):
    c = JobConfig(config, prepend_paths=True)
    assert c.data['input']['filepath_or_buffer'] == 'Z:/data.csv'


def test_jobconfig_command_line_args(config):
    c = JobConfig(config)
    pass
