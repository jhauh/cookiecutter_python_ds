""" Dummy script to show example of general structure and JobConfig. """

from pathlib import Path
from {{ cookiecutter.package_name}}.config import PipelineRunner


def main():
	script_dir = Path(__file__).parent.absolute()

	pipeline_scripts = [
		'a1_create_data.py',
		'b1_print_data.py'
	]

	pr = PipelineRunner(pipeline_scripts, default_script_dir=script_dir)
	pr.run_pipeline()

if __name__ == '__main__':
	main()