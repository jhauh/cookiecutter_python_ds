""" Dummy script to show example of general structure and JobConfig. """

from pathlib import Path
from {{ cookiecutter.package_name}}.config import JobConfig


def main():
    # config files live in config folder in the same location as scripts
    try:
        config_path = Path(__file__).parent.absolute() / 'config' / 'a1_print_data.config'
    except NameError:  # E.g. if running interactively
        config_path = Path('.') / 'config' / 'a1_print_data.config'
    jc = JobConfig(config_path, job_name='print')

    with open(jc.data['input'], 'r') as f:
        data = f.readlines()

    print(data)


if __name__ == '__main__':
    main()
