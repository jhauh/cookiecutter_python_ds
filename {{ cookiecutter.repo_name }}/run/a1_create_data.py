""" Dummy script to show example of general structure and JobConfig. """

from pathlib import Path
from random import randint
from {{ cookiecutter.package_name}}.config import JobConfig


def main():
    # config files live in config folder in the same location as scripts
    try:
        config_path = Path(__file__).parent.absolute() / 'config' / 'a1_create_data.config'
    except NameError:  # E.g. if running interactively
        config_path = Path('.') / 'config' / 'a1_create_data.config'
    jc = JobConfig(config_path, job_name='create')

    data = [randint(1, 100) for i in range(int(jc.n_points))]

    with open(jc.data['output'], 'w') as f:
        for point in data:
            f.write(f'{point}\n')


if __name__ == '__main__':
    main()
