{{ cookiecutter.project_name }} Documentation
====================================================

This folder contains the source files for package documentation. the
``source`` folder is where the content lives, and the ``build`` folder
is the generated html, which is not versioned. The tool of choice for
turning ``source`` into ``build`` is 
`Sphinx <http://www.sphinx-doc.org>`_.

The source material is in ``RST`` format, which is kind of like Markdown
but with way more features. The README files found throughout this repo
are also in RST format, and GitHub/Lab renders them just as nicely as
MD.

Building the docs
-----------------

Run ``make html`` from this folder to trigger a build of the
documentation. You will need to have a version of ``make`` installed or
otherwise available in your ``$PATH``.

There are other file formats which can Sphinx can convert to, see its
documentation for details.