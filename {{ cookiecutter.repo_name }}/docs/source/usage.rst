.. _usage:

User Guide
==========

Scripts
+++++++

Also affectionately known as 'ABC' scripts because of their alphabetical
prefixes, these scripts are the individual pieces of code which make up
the pipelines. They typically take in a data file or files, process them
in some way, and then write the resulting data out again, to be picked
up and ingested by another ABC script.

ABC scripts are found in the ``run/`` folder, and take the
form::

   [letter][number]\_[description].py

Where *letter* denotes the stage at which the script was intended to be
ran (earlier stages get earlier letters), *number* denotes the order
within a given stage scripts should be ran, and *description* is the
english description of what the script does.

To run an individual ABC script, ensure you are in an environment where
``{{ cookiecutter.package_name }}`` is installed, and run (for
example)::

   python a1_create_data.py

Pipelines
+++++++++

These scripts string together ABC scripts, making it easy to refresh
final output files given new data. They are also found in the
``run/`` folder, and are named as::

   run_[name].py

Where *name* is the english description of the output file to be
refreshed. Essentially all they do is call each ABC script in their
pipeline sequentially, ensuring that each one acts on the refreshed
output of the previous. To execute a pipeline, simply run::

   python run_pipeline.py

Since pipelines generally share the same start point (extracting,
cleaning, and preparing the raw data), you may not need to run each
one from the very beginning. In order to skip ahead and start the run at
a later point, the ``+s`` or ``++start`` command line option may be
used. For example::

   python run_pipeline.py +s b

Will run the pipeline, but skip any ABC script with prefix less than 'b'
(i.e. the 'a' steps). This is often useful to reuse previous runs'
steps, which may take a long time to run.

Non-default settings
++++++++++++++++++++

By default, the pipelines, and the ABC scripts they are comprised of,
and the package modules which they are comprised of, make certain
assumptions and decisions about what their output should look like.
These defaults can be changed by the end user via two different methods;
custom config files and/or command line arguments.

Config files
------------

The practical details of where and how data is loaded and saved, and
any user-changeable processing logic, is stored in JSON config files.
The defaults can be found in ``run/config``, and there is one default
config per ABC script.

To change the behaviour or logic of an ABC script, it is recommended to
not edit its default config file, but instead to provide a custom
config. This can be done by copying the default config as a template,
changing the necessary parts, and saving it as a separate file. This
file can be provided to the script at runtime, like so::

   # In general
   python [letter][number]_[description].py --[job_name]__config [config_file.json]
   # E.g.
   python a1_create_data.py --create__config myconfig.json

Where *job_name* is the shorthand name for the job as defined in the
script's :meth:`.JobConfig` instantiation, and *config_file* is your
custom config.

This method can also be used to alter pipeline behaviour, and several
custom configs can be passed to different inner ABC scripts::

   python run_pipeline.py ++args --create__config mycreate.json --print__config myprint.json

Note the use of ``++args`` or ``+a`` to signal that the following
arguments should be passed to the inner scripts.

Command line arguments
----------------------

If you only want to tweak one or two parameters from their defaults, it
can be cumbersome to create a whole config file. Instead, it may be more
convenient to use command line args. The convention for doing so is as
follows::

   python ABCscript.py --job_name__successively__deeper__parameter value

If the job_name part matches ``JobConfig.job_name`` (or ``jc.job_name``
for short), this will set the value of
``jc['successively']['deeper']['parameter']`` to 'value', overwriting any
existing value provided by a config. If the job_name part does not
match, the argument will be ignored. This means that this method can be
used on individual ABC scripts, and through pipelines. For example,
to update the location where ``a1_create_data.py`` writes its output
file::

   python a1_create_data.py --create__data__output /new/location/output.csv

And to update this as part of the ``run_pipeline.py`` pipeline,
including updating the settings of the ABC script which picks this file
up::

   python run_pipeline.py --create__data__output /new/location/output.csv \
   --print__data__input /new/location/output.csv

This method may be combined with custom config files. The hierarchy of
inputs is::

    command line args > command line-provided config > config_path
