API Documentation
=================

This page contains an overview of all the building blocks of the 
``{{ cookiecutter.package_name }}`` package, and links to detailed
desciptions and usage instructions.

.. currentmodule:: {{ cookiecutter.package_name }}

Config
++++++

These classes serve as the glue which sticks all the all the data
processing, clustering, and modelling modules together.

.. autosummary::
   :template: class.rst
   :toctree: api
   :nosignatures:
   
   config.JobConfig
   config.ScriptRunner
   config.PipelineRunner
