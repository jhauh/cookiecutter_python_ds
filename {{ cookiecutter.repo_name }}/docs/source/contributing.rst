Contributing
============

Guidelines to update, extend, or improve the package.

Code
----

If you have changes you'd like to make and share back to the remote
repo, the following steps will help streamline the process:

#. Clone the repo
#. Install package in developer mode
#. Make your changes
#. Test them!
#. Either:

   a. Commit and push back to the remote, or
   b. Open a pull/merge request if you'd like to discuss before merging
      back.


Documentation
-------------

The docs for the project are built using Sphinx, a python tool for 
generating "living documentation". It extracts module/class/function
signatures and descriptions from the source code, and produces 
documentation in a variety of output formats. This allows for a single
source of truth within documentation (no more making sure the Word doc
stays in sync with the Confluence page), and means that the docs live
close to (read:inside) the code.  

There are two broad types of documentation:

#. Static docs, whose content lives in ".rst" files inside the 
   ``docs/source`` folder of the repo. For example, this page, or 
   :ref:`install`.
#. Living docs, whose content is primarily defined within docstrings 
   inside the package source. E.g.
   :meth:`~{{ cookiecutter.package_name }}.config.JobConfig`.

And two ways you can contribute to the documentation:

a) Amend an existing page or docstring.
#) Create a new page or documented module/class/function.

If your changes fall under a), all you need to do is make your changes
in the correct place, regenerate the docs with ``make html``, and
iterate until you're happy. Then simply commit your changes as above.

If the proposed changes fall under b), the process is slightly more
involved. First of all, create the new page or code item in question.
Then you will need to add it to one of the "table of contents trees" (or
"toctrees") in the docs.  

For static docs, add the new rst filename to 
``index.rst`` wherever it fits best.  
For new code, add the module/class/function name to the correct section
in ``api.rst``. On regeneration, this will create a new page which 
parses the relevant docstrings and signatures.

Regenerate and iterate until happy, and then commit everything to the
remote as above.


Tests
-----

You can never have too many tests!  

``pytest`` is our testing framework of choice, but the written tests 
*should* work with most other Python testing packages, such as 
``unittest`` and ``nose``.  

To add your own tests, just put them into the ``test`` folder, either 
in an existing test script or in a new one named ``test_``-something, 
and containing classes, methods, or functions also containing the word
``test``. Pytest will find them automatically 99% of the time.  

For examples of tests, test fixtures, and test parameters, see 
``test_JobConfig.py``.
