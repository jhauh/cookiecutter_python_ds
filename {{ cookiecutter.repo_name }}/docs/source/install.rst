.. _install:

Installation
============

The code is provided as a python package as part of a Git repository.
This page provides instructions on how to install everything from
scratch.

1. Clone this repo
------------------

2. Create a virtual environment
-------------------------------

Although not *strictly* necessary, it is considered poor form to clog up
the base Python environment with things which aren't broadly useful
across projects (like Jupyter, for example). Therefore, it is
recommended that each distinct project live in its own environment,
which enables the use of different sets of packages and package
versions. At this point the only dependency to be aware of is that the
package has been developed on **Python
{{ cookiecutter.minimum_python_version }}**. If you don't have
an existing environment to install the package into, create one. The 
easiest way to do this for this project is with 
`Poetry <https://python-poetry.org/>`_ (though conda and similar tools
are also fine), which makes use of the the included ``pyproject.toml``::

    poetry install

Will use this file to create an environment with all required 
dependencies. This environment can be entered with the command::

    poetry shell

3. Validate installation
------------------------

From inside the environment, open up a REPL and test that you can::

    import {{ cookiecutter.package_name }}

If so, you're good to go! Read more about how to use the package and
codebase in :ref:`usage`.
