""" Abstract boilerplate & runtime settings from codebase """

from argparse import ArgumentParser
import collections
from functools import wraps as _wraps
import json
from pathlib import Path
import re
from subprocess import PIPE, run
import time
import jsonschema


class JobConfig:
    """ General-purpose script configuration handler.

    This class is designed to contain all the job-specific options for
    a given script. For example, the location of data to use, the
    location to output results to, or any other tunable parameters you
    want to expose. It aims to provide a consistent, concise interface
    for configuration, keeping real code tidy with minimal boilerplate.

    At its heart it's just a dictionary with some tooling around
    combining options provided via dict, json, and command line.

    Args:
        job_config (str or dict): Location of json to load as config,
            or the pre-loaded dict to attach as config.
        job_name (str or None): Shorthand name for the given job, '*',
            or None. Used only for filtering to relevant command line
            args (necessary for pipelines where all arguments are passed
            to all scripts). Defaults to None, which ignores all
            arguments. A value of '*' will use all arguments. A value of
            'job_name' will select only those arguments beginning with
            'job_name'. See :meth:`parse_args` for more info.
        validate_config (bool): Whether or not to validate the schema of
            the provided config file to ensure its shape is suitable for
            the current job. Defaults to False.
        schema (dict): Used to validate the provided config if
            ``validate_config`` is True. If none is provided, falls back
            to a default schema which tests only for the presence of a
            top-level ``'data'`` entry.
        prepend_paths (bool): Should given data locations be prefixed
            with the value specified in the top-level ``'prefix'``
            entry? Useful to reduce repetition in filepaths. Defaults to
            False.

    Returns:
        On instantiation, loads/attaches/grabs configuration parameters
        from json files/dicts/command line arguments, and returns a
        subscriptable object through which parameters can be accessed.

    The hierarchy of inputs is::

        command line args > command line-provided config > config_path

    Therefore, if a config file is provided via command line argument
    (e.g. ``--job_name__config``, this will take precedence over that
    provided by ``config_path``. Similarly, any individual option
    provided via command line (e.g. ``--job_name__prefix``) will
    overwrite that provided by any config.

    Example:
        >>> jc = JobConfig({'data': 'folder/data.csv', 'prefix': 'Z:/'})
        >>> pd.read_csv(jc['data'])  # or jc.data
           a  b
        0  1  4
        1  2  5
        2  3  6
    """
    def __init__(self, job_config=None, job_name=None,
                 validate_config=False, schema=None, prepend_paths=False):
        """ Assign config variables to instance of class. """
        self.job_name = job_name
        self.validate_config = validate_config
        self.schema = schema or self._default_config_schema
        self.prepend_paths = prepend_paths
        self.args, args_config = self.parse_args()
        self.job_config = args_config or job_config
        for k, v in self._parse_config().items():
            setattr(self, k, v)
        del self.schema

    def __getitem__(self, item):
        """ This makes the instance subscriptable e.g. c['data'] """
        return getattr(self, item)

    def __repr__(self):
        """ Give useful output when evaluated """
        return str(self.__dict__)

    def __str__(self):
        """ Give useful & pretty output when printed """
        return json.dumps(self.__dict__, indent=2)

    @property
    def _default_config_schema(self):
        schema = {
            "$schema": "http://json-schema.org/draft-07/schema#",
            "title": "Default",
            "description": "Minimal job config schema. Checks for 'data' entry",

            "required": ["data"],
            "type": "object",
            "properties": {
                "data": {
                    "type": "object",
                    "patternProperties": {
                        "^.*$": {"$ref": "#/definitions/dataset"}
                    },
                    "minProperties": 1
                }
            },

            "definitions": {
                "dataset": {
                    "type": "object",
                    "anyOf": [
                        {
                            "required": ["filepath_or_buffer"],
                            "properties": {"filepath_or_buffer": {"type": "string"}}
                        },
                        {
                            "required": ["path_or_buf"],
                            "properties": {"path_or_buf": {"type": "string"}}
                        },
                        {
                            "required": ["fname"],
                            "properties": {"fname": {"type": "string"}}
                        }
                    ]
                }
            }
        }
        return schema

    def _prepend_drive_to_paths(self, opts, prefix_key='prefix'):
        """ Add user/drive-specific prefixes to relevant paths """
        for dataset in opts['data']:
            if isinstance(opts['data'][dataset], dict):
                # Rewrite to get kwarg names from schema automatically
                # Reading in
                if 'filepath_or_buffer' in opts['data'][dataset].keys():
                    opts['data'][dataset]['filepath_or_buffer'] = (
                        opts[prefix_key]
                        + opts['data'][dataset]['filepath_or_buffer'])
                elif 'fname' in opts['data'][dataset].keys():
                    opts['data'][dataset]['fname'] = (
                        opts[prefix_key]
                        + opts['data'][dataset]['fname'])
                # Writing out
                elif 'path_or_buf' in opts['data'][dataset].keys():
                    opts['data'][dataset]['path_or_buf'] = (
                        opts[prefix_key]
                        + opts['data'][dataset]['path_or_buf'])
        return opts

    def parse_args(self):
        """ Flexibly (read:manually) process command line args.

        This method handles all command line arguments passed to the
        ``JobConfig`` script. It requires no prior knowledge of the
        arguments passed, instead parsing each option manually and
        deciding whether to keep or throw away the option based on its
        name. This method is public in order to demonstrate usage of
        command line arguments with ``JobConfig``.

        The convention adopted here is as follows::

            python script_with_jobconfig.py --job_name__successively__deeper__parameter value

        If the job_name part matches ``JobConfig.job_name`` (or
        ``jc.job_name`` for short), this will set the value of
        ``jc['successively']['deeper']['parameter']`` to 'value',
        overwriting any existing value provided by a config. If the
        job_name part does not match, the argument will be ignored. On
        that note, configs are special, and can be provided as::

            python script_with_jobconfig.py --job_name__config config.json

        Config files and individual parameters may be passed this way
        simultaneously, with individual parameters taking precendence.

        Note that the default argument prefix here is set to '+', so a
        script can be ran as follows to provide helpful instructions:

            >>> python script_with_jobconfig.py ++help
            usage: script_with_jobconfig.py [+h] [args [args ...]]
            Pass arguments to job_name
            positional arguments:
                args        Just pass whatever you like to this script
            optional arguments:
                +h, ++help  show this help message and exit

        """
        if self.job_name is None:
            # Ignore all command line args
            return None, None
        elif self.job_name == '*':
            # Accept all command line args. Be careful with collisions
            self.job_name = ''
        # First, gather all arguments into a list
        parser = ArgumentParser(description='Pass arguments to ' + self.job_name,
                                prefix_chars='+')
        parser.add_argument('args', nargs='*',
                            help='Just pass whatever you like to this script')
        args = parser.parse_args().args
        # If empty, end it here
        if not args:
            return None, None
        # Second, sort them into (arg, value pairs)
        args = self._gather_args(args)
        # Third, filter to arguments relevant to this script
        job_args = {k: v for k, v in args.items() if k.startswith(self.job_name)}
        args_config = job_args.pop(self.job_name + '__config', None)
        # Fourth, use the underscore notation to nest the dictionary
        job_args = self._nest_args(job_args)
        return job_args, args_config

    def _gather_args(self, args):
        """ Convert lists of named arguments into a usable dictionary"""
        new_parser = ArgumentParser()
        for arg in args:
            if re.search('^--?', arg):
                new_parser.add_argument(arg, nargs='*')
        dynamic_args = new_parser.parse_args(args).__dict__
        # Above saves all values as lists, extract values if lonely
        for arg in dynamic_args.keys():
            if len(dynamic_args[arg]) == 1:
                dynamic_args[arg] = dynamic_args[arg][0]
        return dynamic_args

    def _nest_args(self, arg_dict):
        """ Unflatten dictionary to reproduce structure found in config

        '__' is the standard convention for passing to a deeper level
        """
        nested_args = dict()
        for key in arg_dict.keys():
            key_dict = dict()
            # Discard top level used to denote relevant script
            levels = key.split('__')[1:]
            # If  parameter is at the top level of config, set directly
            if len(levels) == 1:
                key_dict[levels[0]] = arg_dict[key]
            # If parameter is nested, get structure from underscores
            else:
                inner_dict = {levels[-1]: arg_dict[key]}
                for level in reversed(levels[1:-1]):
                    inner_dict = {level: inner_dict}
                key_dict[levels[0]] = inner_dict
            nested_args = self._merge_dicts(nested_args, key_dict)
        return nested_args if nested_args != {} else None

    def _merge_dicts(self, a, b, tree_path=None):
        """ Merges b into a non-destructively"""
        if tree_path is None:
            tree_path = []
        for key in b:
            if key in a:
                if isinstance(a[key], dict) and isinstance(b[key], dict):
                    self._merge_dicts(a[key], b[key], tree_path + [str(key)])
                elif a[key] == b[key]:
                    pass  # same leaf value
                else:
                    raise Exception('Conflict at %s' % '.'.join(tree_path + [str(key)]))
            else:
                a[key] = b[key]
        return a

    def _update_dict(self, d, u):
        """ Updates one dictionary with another non-destructively"""
        for k, v in u.items():
            if isinstance(v, collections.Mapping):
                d[k] = self._update_dict(d.get(k, {}), v)
            else:
                d[k] = v
        return d

    def _parse_config(self):
        """ Take everything in the config json """
        if isinstance(self.job_config, dict):
            opts = self.job_config
        elif isinstance(self.job_config, str):
            with open(self.job_config, 'r') as c:
                opts = json.load(c)
        else:
            raise ValueError(
                "Can't parse config of type {}".format(type(self.job_config)))
        if self.validate_config is True:
            jsonschema.validate(opts, self.schema)
        # Overwrite config parameters with command line ones, if any
        if self.args is not None:
            self._update_dict(opts, self.args)
        if self.prepend_paths is True:
            opts = self._prepend_drive_to_paths(opts)
        return opts


def timer(func):
    """ Decorator for ``ScriptRunner`` run methods.

    Measures and prints time taken for function/method to execute.
    """
    @_wraps(func)  # Makes the decorator well behaved for docstrings
    def timed(*args, **kwargs):
        start = time.time()
        res = func(*args, **kwargs)
        print('Time taken:', int(time.time() - start), 'seconds\n')
        return res
    return timed


class ScriptRunner:
    """ Run arbitrary python scripts.

    Handles running, timing, and graceful erroring of python scripts.
    Useful for ensuring scripts are ran in a consistent manner, and
    terminates parent process if they fail, avoiding further execution.

    Args:
        python_interpreter (str): The location of the python interpreter
            used to execute any scripts. Defaults to 'python', the
            version obtained by running ``which python`` in the shell.
        default_script_dir (str): The location of any named script to be
            ran by ``run_script``. Defaults to '.', which assumes
            scripts are in the same directory the ``ScriptRunner``
            script was run from.
    """
    def __init__(self, python_interpreter='python', default_script_dir='.'):
        self.python_interpreter = python_interpreter
        self.default_script_dir = default_script_dir

    @timer
    def run_script(self, script, *opts):
        """ Method to individual python scripts.

        Args:
            script (str): The python script to execute.
            *opts (list of str): Any additional options to be passed to
                ``script``.

        Returns:
            NoneType: If the provided script executed successfully, else
            ValueError: Detailed errors will be printed if it fails.
        """
        script_fullpath = Path(self.default_script_dir) / script
        print('Running ' + script)
        o = run([self.python_interpreter, script_fullpath] + list(opts),
                stdout=PIPE, stderr=PIPE)  # .decode('utf-8')
        if o.returncode > 0:
            raise ValueError(script + ' failed with error code ' +
                             str(o.returncode) + ':\n' + o.stderr.decode('utf-8'))
        print(o.stdout.decode('utf-8'))


class PipelineRunner(ScriptRunner):
    """ Run sequences of python scripts.

    Given a list of python scripts, execute them sequentially. Also
    handles command line argument to choose an point in the pipeline to
    start from, and arguments to be passed to the scripts to be ran. See
    ``parse_args`` for more information on options.

    Args:
        pipeline (list of str): Ordered list of jobs to run.
        start_point (str): Point in the pipeline to start running from.
            One of {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', None}.
            Defaults to None. See belowfor more info.
        python_interpreter (str): The location of the python interpreter
            used to execute any scripts. Defaults to 'python', the
            version obtained by running ``which python`` in the shell.
        default_script_dir (str): The location of any named script to be
            ran by ``run_script``. Defaults to '.', which assumes
            scripts are in the same directory the ``ScriptRunner``
            script was run from.

    While this class can be used to run arbitrary sequences of python
    scripts, the functionality afforded by ``start_point`` means that it
    is advantageous to adhere to an alphabetical naming convention.
    With a pipeline of ``['a_job1.py', 'b_job2.py', 'c_job3.py']``,
    the ``start_point`` parameter can be used to skip the initial jobs
    if required.

    Example:
          >>> pr = PipelineRunner(['a_job1.py', 'b_job2.py', 'c_job3.py'], start_point='b')
          >>> pr.run_pipeline()
          Pipeline to run: ['b_job2.py', 'c_job3.py']
          Arguments to pass: []
          Running b_job2.py
          Time taken: 2 seconds
          Running c_job3.py
          Time taken: 10 seconds
    """
    def __init__(self, pipeline, start_point=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.args = self.parse_args()
        self.start_point = self.args.start or start_point
        self.pipeline = (
            self._filter_pipeline(pipeline) if
            self.start_point is not None else pipeline
        )
        print('Pipeline to run:', ' --> '.join(self.pipeline))
        print('Arguments to pass:', self.args.args)

    def parse_args(self):
        """ Argument parser to split pipeline and pipeline script args.

        Accepts a combination of pipeline-related arguments and args to
        be passed to pipeline scripts. Note the non-default prefix of
        '+', used to disambiguate between pipeline related args and
        args to pass down to pipeline scripts.

        All passable arguments are passed to all scripts, which must
        individually process the whole list and pick out those relevant
        to them.

        Example:
            >>> python pipeline_script.py ++start c ++args --job1_name__config job1.json __job2_name__config job2.json

        """
        parser = ArgumentParser(description='Generic parser for run_* scripts',
                                prefix_chars='+')
        parser.add_argument('+s', '++start', type=str, default=None,
                            choices={'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'},
                            help='Point in the pipeline to start the run from')
        parser.add_argument('+a', '++args', nargs='*', default=[],
                            help='All other arguments to be passed down to abc scripts')
        return parser.parse_args()

    def _filter_pipeline(self, pipeline):
        """ Skip ahead to later points in pipeline"""
        pipeline = [job for job in pipeline if job[0] >= self.args.start]
        return pipeline

    def run_pipeline(self):
        """ Main driver method for the class.

        Sequentially calls :meth:`run_script` on each pipeline script.
        """
        for job in self.pipeline:
            self.run_script(job, *self.args.args)
