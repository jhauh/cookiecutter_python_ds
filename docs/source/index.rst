.. Cookiecutter-ds-python documentation master file, created by
   sphinx-quickstart on Thu Feb 27 20:34:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cookiecutter-ds-python's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Read the 
`original project's docs <http://drivendata.github.io/cookiecutter-data-science/>`_
to convince yourself why we should all use a cookiecutter, then read 
`this project's README <https://github.com/fluffactually/cookiecutter-ds-python>`_
to convince yourself why to use this one. 